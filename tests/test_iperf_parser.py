from iperf3_to_csv.parser import IPerfParser


class TestIperfParser:
    measurements = ''

    def setup_class(cls):
        with open('test_measurement.txt') as f:
            TestIperfParser.measurements = f.read()

    def test_read_file(self):
        parser = IPerfParser()
        iperf_file = parser.parse(TestIperfParser.measurements)
        assert len(iperf_file.intervals) == 30
        assert iperf_file.intervals[0].end_time == 1
        assert iperf_file.intervals[1].start_time == 1
        assert iperf_file.intervals[0].bitrate == 10100000
        assert iperf_file.intervals[0].omitted == True
        assert iperf_file.intervals[12].omitted == False

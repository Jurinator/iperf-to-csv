import os
from pathlib import Path

from iperf3_to_csv.parser import IPerfParser

directory = 'Your_dir'
parser = IPerfParser()
separator = ';'
for filename in os.listdir(directory):
    file_path = Path(os.path.join(directory, filename))
    if os.path.isfile(file_path):
        csv = f'start time{separator}bitrate in bits/s{separator} omitted\n'
        with open(file_path) as file:
            iperf_file = parser.parse(file.read())

            for intervall in iperf_file.intervals:
                csv += f'{intervall.start_time}{separator}{intervall.bitrate}{separator}{intervall.omitted}\n'
        with open(f'{file_path.stem}.csv', 'w') as outputfile:
            outputfile.write(csv)


import re
from typing import List


class IPerfInterval:
    def __init__(self):
        self.start_time = 0
        self.end_time = 0
        # in bits/s
        self.bitrate = 0
        self.omitted = False


class IperfFile:
    def __init__(self):
        self.intervals: List[IPerfInterval] = list()
        self.data_rate = 0


class IPerfParser:
    def parse(self, text: str) -> IperfFile:
        file = IperfFile()
        regex_for_sum_line = f'\[SUM\]\s*(\d*\.*\d*)-(\d*\.*\d*)\D*(\d*\.\d*).*  (\d*\.*\d*)(.*)\/sec'
        lines = text.split('\n')
        for line in lines:
            search = re.search(regex_for_sum_line, line)
            if search:
                interval = IPerfInterval()
                interval.start_time = float(search.groups()[0])
                interval.end_time = float(search.groups()[1])
                bitrate_value = float(search.groups()[3])
                unit = search.groups()[4]
                if 'mbits' in unit.lower():
                    interval.bitrate = bitrate_value*1000000
                if 'kbits' in unit.lower():
                    interval.bitrate = bitrate_value*1000
                interval.omitted = 'omitted' in line
                file.intervals.append(interval)
        file.data_rate = file.intervals[-1].bitrate
        del file.intervals[-2:]

        return file
